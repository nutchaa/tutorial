<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Import Vue3js -->
    <script src="https://unpkg.com/vue@next"></script>

    <!-- Import axios -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>


    <title>Hello, world!</title>
</head>

<body>
    <div class="container" id="app">


        <h2 align="center">{{message}}</h2>
        <div class="row">
            <div class="col-md-12">
                <form @submit="submitData" @reset="resetData" action="" method="post">
                    <div class="form-group">
                        <label for="">ชื่อจริง</label>
                        <input type="text" class="form-control" v-model="form.fname" name="">
                    </div>

                    <div class="form-group">
                        <label for="">นามสกุล</label>
                        <input type="text" class="form-control" v-model="form.lname" name="">
                    </div>

                    <input type="submit" v-model="form.status" class="btn btn-success">
                    <input type="reset" value="ยกเลิก" class="btn btn-danger">
                </form>
            </div>
        </div>

        <div class="py-2">
            {{form}}
        </div>

        <table class="table table-bordered table-hover table-strip">
            <thead class="text-center">
                <tr class="table-success">
                    <th scope="col">#ID</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody class="text-center">
                <tr v-for="row in users" :key="row.id">
                    <th scope="row">{{row.id}}</th>
                    <td>{{row.fname}}</td>
                    <td>{{row.lname}}</td>
                    <td>
                        <button class="btn btn-info" @click="editUser(row.id)">แก้ไข</button>
                    </td>
                    <td>
                        <button class="btn btn-danger" @click="DeleteUser(row.id)">ลบ</button>
                    </td>
                </tr>
            </tbody>
        </table>


    </div>




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>

    <!-- import controller -->
    <!-- include Mount App -->
    <script src="app.js"></script>

</body>

</html>