<?php
	$serverName = "localhost";
	$userName = "root";
	$userPassword = "";
	$dbName = "vue-php";

	$conn = new PDO("mysql:host=$serverName;dbname=$dbName", $userName, $userPassword);
    $conn->exec("set names utf8");

    //รับค่าที่ส่ง
    $requestData = json_decode(file_get_contents("php://input"));

    $data = [];
    $output = [];

    //---------------------------------------------------------------------------------------------------------

    if($requestData->action == "insert"){

        $data = [
            ":fname" => $requestData->fname,
            ":lname" => $requestData->lname,
        ];

        $query = "INSERT INTO users (fname,lname) VALUES (:fname,:lname)";
        $stmt = $conn->prepare($query);
        $stmt->execute($data);

        $output = [
            "message" => "Insert Complete !"
        ];
        echo json_encode($output);
    }

    //---------------------------------------------------------------------------------------------------------

    if($requestData->action == "getAll"){

        $query = "SELECT * FROM users";
        $stmt = $conn->prepare($query);
        $stmt->execute();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $data[] = $row;
        }

        echo json_encode($data);
    }

    //---------------------------------------------------------------------------------------------------------

    if($requestData->action == "getEditUser"){

        $data = [
            ":id" => $requestData->id,
        ];

        $query = "SELECT * FROM users WHERE id = :id";
        $stmt = $conn->prepare($query);
        $stmt->execute($data);

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $data["id"] = $row["id"];
            $data["fname"] = $row["fname"];
            $data["lname"] = $row["lname"];
        }

        echo json_encode($data);
    }

    //---------------------------------------------------------------------------------------------------------

    if($requestData->action == "EditUser"){

        $data = [
            ":id" => $requestData->id,
            ":fname" => $requestData->fname,
            ":lname" => $requestData->lname
        ];

        $query = "UPDATE users SET fname = :fname , lname = :lname WHERE id = :id";
        $stmt = $conn->prepare($query);
        $stmt->execute($data);

        $output = [
            "message" => "Edit Complete !"
        ];

        echo json_encode($output);

    }

    //---------------------------------------------------------------------------------------------------------

    if($requestData->action == "DeleteUser"){

        $data = [
            ":id" => $requestData->id,
        ];

        $query = "DELETE FROM users WHERE id = :id";
        $stmt = $conn->prepare($query);
        $stmt->execute($data);

        $output = [
            "message" => "Delete Complete !"
        ];

        echo json_encode($output);

    }

    $conn = null;
?>