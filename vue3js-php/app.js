const app = Vue.createApp({
    data(){
        return {
            conn:"action.php",
            users:"", //สร้างตัวแปรเก็บข้อมูล Users จากที่ getAll มา

            mhee: ['test1','test2'],

            message: 'Vuejs & PHP (Single Page Application)',
            form:{
                id:"",
                fname:"",
                lname:"",
                isEdit:false, //check การแก้ไขข้อมูล
                status:"บันทึก"
            }
        }
    },
    methods: {
        submitData(e){
            e.preventDefault()
            if(this.form.fname != '' && this.form.lname != ''){
                if(!this.form.isEdit){ //<-- ถ้าเป็น false !
                    //บันทึกข้อมูล
                    axios.post(this.conn,{
                        fname:this.form.fname,
                        lname:this.form.lname,
                        action:"insert"
                    }).then(function(res){
                        console.log(res) // หรือ res.data (มาจากไฟล์ db connect   )
                        app.resetData() // reset ข้อมูล ตามด้วยดึงข้อมูลใหม่ ->
                        app.getAllUsers()
                    })
                } else {
                    //แก้ไขข้อมูล
                    axios.post(this.conn,{
                        id:this.form.id,
                        fname:this.form.fname,
                        lname:this.form.lname,
                        action:"EditUser"
                    }).then(function(res){
                        console.log(res) // หรือ res.data (มาจากไฟล์ db connect   )
                        app.resetData() // reset ข้อมูล ตามด้วยดึงข้อมูลใหม่ ->
                        app.getAllUsers()
                    })
                }
            } else {
                alert('กรุณากรอกข้อมูลให้ครบถ้วน');
            }
        },
        resetData(e) {
            // e.preventDefault()
            this.form.id = ""
            this.form.fname = ""
            this.form.lname = ""
            this.form.isEdit = false
            this.form.status = "บันทึก"
        },
        getAllUsers(){
            //เรียกข้อมูลมาแสดง
            axios.post(this.conn,{
                action:"getAll"
            }).then(function(res){
                app.users = res.data
                console.log(app.users)
            })
        },
        editUser(id){
            this.form.isEdit = true
            this.form.status = "อัพเดท"

            axios.post(this.conn,{
                id:id,
                action:"getEditUser"
            }).then(function(res){
                // console.log(res)
                app.form.id = res.data.id
                app.form.fname = res.data.fname
                app.form.lname = res.data.lname
            })

        },
        DeleteUser(id){

            if(confirm("คุณต้องการลบข้อมูลรหัส "+id+" ใช่หรือไม่ ?")){
                axios.post(this.conn,{
                    id:id,
                    action:"DeleteUser"
                }).then(function(res){
                    console.log(res) // หรือ res.data (มาจากไฟล์ db connect   )
                    app.resetData() // reset ข้อมูล ตามด้วยดึงข้อมูลใหม่ ->
                    app.getAllUsers()
                })
            }

        }
    },
    created(){
        this.getAllUsers();
    }
}).mount('#app')