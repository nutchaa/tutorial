const db = firebase.firestore()

const table = document.querySelector('#tbresult')

const form = document.querySelector('#addForm')

// db.collection("user").get().then((snapshot)=>{
//     // console.log(snapshot.docs);
//     snapshot.forEach(doc=>{
//         // console.log(doc.data())
//         showData(doc)
//     })
// })

db.collection("user")
// .where("city","==","Bangkok")
// .orderBy("age","desc")
.get()
.then((snapshot)=>{
    // console.log(snapshot.docs);
    snapshot.forEach(doc=>{
        // console.log(doc.data())
        showData(doc)
    })
})

form.addEventListener('submit',(e)=>{
    e.preventDefault()
    // console.log(form.name.value)
    // console.log(form.age.value)
    // console.log(form.city.value)

    db.collection("user").add({
        name: form.name.value,
        city: form.city.value,
        age: form.age.value
    })
    .then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);

        form.name.value = ''
        form.city.value = ''
        form.age.value = ''
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });

})

function showData(doc){
    var row = table.insertRow(-1)
    var cell1 = row.insertCell(0)
    var cell2 = row.insertCell(1)
    var cell3 = row.insertCell(2)
    var cell4 = row.insertCell(3)
    var cell5 = row.insertCell(4)

    cell1.innerHTML = doc.data().name
    cell2.innerHTML = doc.data().city
    cell3.innerHTML = doc.data().age

    let btn = document.createElement('button')
    btn.textContent = "ลบข้อมูล"
    btn.setAttribute("class","btn btn-danger")
    btn.setAttribute("data-id",doc.id)

    let btn2 = document.createElement('button')
    btn2.textContent = "แก้ไขข้อมูล"
    btn2.setAttribute("class","btn btn-warning")
    btn2.setAttribute("data-id",doc.id)

    cell4.appendChild(btn)
    cell5.appendChild(btn2)


    btn.addEventListener("click",(e)=>{
        let id = e.target.getAttribute("data-id")

        db.collection("user").doc(id).delete()

    })

    btn2.addEventListener("click",(e)=>{
        let id = e.target.getAttribute("data-id")

        db.collection("user").doc(id).update({
            name: "eiei",
            city: "eiei",
            age: "0"
        })

    })
}

